Some python scripts and command line tools for nenufar.


-----

- `nearfield_BF.py` : compute and plot a map of the near field zone, based on scalar beamforming taking sphericity of incoming wavefronts.

    *Shell script:*
    
```shell   
    ./nearfield_BF.py -h
usage: nearfield_BF.py [-h] [-t TIME_INDEX] [-f FREQ] [-Nx NXPIX] [-Ny NYPIX]
                       [-d DIMS [DIMS ...]] [-p POLAR] [-H HEIGHT] [-o OUTPUT]
                       [-l]
                       filename

Compute image of the near field from a nenufar xst fits file.

positional arguments:
  filename              Full path to xst fits file

optional arguments:
  -h, --help            show this help message and exit
  -t TIME_INDEX, --time_index TIME_INDEX
                        Time index to be plotted
  -f FREQ, --freq FREQ  Frequency to be plotted (Hz)
  -Nx NXPIX, --Nxpix NXPIX
                        Nb of pixels along x dimension
  -Ny NYPIX, --Nypix NYPIX
                        Nb of pixels along y dimension
  -d DIMS [DIMS ...], --dims DIMS [DIMS ...]
                        Limits of the picture in meters, with syntax: -d xmin
                        xmax ymin ymax
  -p POLAR, --polar POLAR
                        Polarization X or Y (0 or 1)
  -H HEIGHT, --height HEIGHT
                        Height at which image is computed
  -o OUTPUT, --output OUTPUT
                        Output picture file (type set by extension through
                        `savefig()`)
  -l, --linear          Plot linear power instead of log

```



    *Python module functions: see doc/api.pdf*

Example map @65MHz (not calibrated):

![](test2.png)