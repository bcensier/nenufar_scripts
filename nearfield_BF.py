#!/usr/bin/env python
# -*- coding: utf-8 -*-

import scipy.constants as const
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap
import sys
import fitsio
import argparse
import pyproj

import pdb

# Benjamin Censier (USN Nancay) 03/2019, GPLv3 or later license
#       This is free software: you are free to change and redistribute it.  There is NO WARRANTY, to the extent permitted by law.


# TODO: 
# - speedup with parallelization
# - read one subband /one polar/ one time index only in the file -/- specify a set of subband/time index/polar to be computed in a row
# - plot results on a map
# - include calibration table from the command line



################### near field imaging (scalar beamforming) #########################################

# Original code in lofarimaging ipython notebook git repo: 'https://github.com/cosmicpudding/lofarimaging/blob/master/LOFAR LBA imaging tutorial v4.ipynb'#


def ground_imager(visibilities,freq,im_x,im_y,dims,station_pqr,height=1.5,plotfig=False):

    """
    Compute an image of near field based on correlation matrix and antenna positions

    Inputs:
         visibilities: correlation matrix NxN
         freq: frequency (Hz)
         im_x (resp. im_y): number of pixels along x dimension (resp. y)
         dims: list of image limits [xmin, xmax, ymin, ymax] (m)
         station_pqr: mini arrays coordinates Nx3 in geocentric coordinates
         height: height at which the nearfield is evaluated (optinal, default = 1.5m)
         plotfig: plot figure or not (optional, default = False)

    Outputs:
         img: image of nearfield in matrix
         dims: copy of input dims 
         dims_lonlat: dims in lon/lat [lon(A),lat(A),lon(B),lat(B)], where A = lower left corner and B = upper right corner
         MRpos_loc: mini arrays positions in local ENU 
    """
    


    img = np.zeros([im_y,im_x], dtype='float32')

    N = img.shape[0]*img.shape[1]


    baryc_geoc = np.mean(station_pqr,axis=0) # barycenter of geocentric coordinates

    station_pqr, lon_normal, lat_normal, Mrot = geocentric_to_local(station_pqr)
#    station_pqr = station_pqr - np.mean(station_pqr,axis=0)
    
    lon0, lat0, height0 = geocentric2lonlat(baryc_geoc[0],baryc_geoc[1],baryc_geoc[2]) # lon/lat for barycenter

    
    xyz_geoc_SW = local_to_geocentric(np.array([dims[0],dims[2],height]).reshape(1,3),lon0,lat0,height0)
    xyz_geoc_NE = local_to_geocentric(np.array([dims[1],dims[3],height]).reshape(1,3),lon0,lat0,height0)    

    lonlat_SW = geocentric2lonlat(xyz_geoc_SW[0,0],xyz_geoc_SW[0,1],xyz_geoc_SW[0,2])
    lonlat_NE = geocentric2lonlat(xyz_geoc_NE[0,0],xyz_geoc_NE[0,1],xyz_geoc_NE[0,2])

    dims_lonlat = [lonlat_SW[0],lonlat_NE[0],lonlat_SW[1],lonlat_NE[1]] 


    print "> Computing near field map..."
    Ndash = 40
    sys.stdout.write(">"+"-"*Ndash+" [0%]\r")
    itot = 0
    
    for q_ix,q in enumerate(np.linspace(dims[2],dims[3],im_y)):
        for p_ix,p in enumerate(np.linspace(dims[0],dims[1],im_x)):
            r = height
            pqr = np.array([p,q,r], dtype='float32')
            antdist = np.linalg.norm(station_pqr - pqr[np.newaxis,:],axis=1)
            groundbase = np.zeros([len(station_pqr),len(station_pqr)], dtype='float32')
            for i in range(0,len(station_pqr)):
                groundbase[i] = antdist[i] - antdist[:]
            img[q_ix,p_ix] = np.mean(visibilities*np.exp(-2j*np.pi*freq*(-groundbase)/299792458.0)).real

            itot+=1
            frac_done = float(itot)/N
            Ndone = int(np.round(Ndash*frac_done))

            sys.stdout.flush()
            sys.stdout.write("+"*Ndone+">"+"-"*(Ndash-Ndone-1)+" ["+"%d"%(int(frac_done*100))+"%]\r")

            
    if plotfig:

        plt.figure()
        plt.imshow(10*np.log10(img.real/np.max(img.real.flatten())),interpolation='None',extent=dims,origin='lower')
        cl = plt.colorbar()
        cl.set_label('dB (norm to max)')

        plt.scatter(station_pqr[:,0],station_pqr[:,1], label="Antenna positions")
        plt.legend()
        plt.xlabel(r"West $\leftarrow$ x (m) $\rightarrow$ East")
        plt.ylabel(r"South $\leftarrow$ y (m) $\rightarrow$ North")
        plt.title('Ground imaging with near-field focalisation \n F = '+"%.3f"%(freq/1e6)+" MHz, "+"%d"%im_x+"x"+"%d"%im_y+" pixels , pixel size = "+"%.1f"%((dims[1]-dims[0])/im_x)+" m")
        plt.show()


    return img, dims, dims_lonlat, station_pqr




################### Coord transformations #########################################

def ECEF_to_ENU_rot_matrix(lon,lat):
	"""
	From the given (lon,lat) position in degrees, output the rotation matrix M allowing the tranformation between x,y,z in geocentric ECEF coordinates and x',y',z' in local East North Up (ENU) coordinates:
	x'      x
	y' = M  y
	z'      z

	see e.g. http://www.navipedia.net/index.php/Transformations_between_ECEF_and_ENU_coordinates
		 https://en.wikipedia.org/wiki/Axes_conventions#/media/File:ECEF_ENU_Longitude_Latitude_relationships.svg
	"""


	lamb = lon/180*np.pi
	phi = lat/180*np.pi

	M = np.zeros((3,3))
	M[0,0] = -np.sin(lamb)
	M[0,1] = np.cos(lamb)

	M[1,0] = -np.cos(lamb)*np.sin(phi)
	M[1,1] = -np.sin(lamb)*np.sin(phi)
	M[1,2] = np.cos(phi)

	M[2,0] = np.cos(lamb)*np.cos(phi)
	M[2,1] = np.sin(lamb)*np.cos(phi)
	M[2,2] = np.sin(phi)


	return M





def geocentric_to_local(MRpos):
    """
    From x,y,z of N elements stored in the Nx3 array MRpos in geocentric cartesian coords, outputs x,y,z in the local ENU coords (MRpos_loc Nx3 array), longitude and latitude of barycenter, and rotation matrix from geocentric to local.
    The barycenter of antennas position is computed in geocentric coords, the corresponding lon/lat will serve defining a rotation matrix Mrot from geocentric to local ENU coords.
    """
        
    baryc = np.mean(MRpos,axis=0)
    lon0, lat0, height = geocentric2lonlat(baryc[0],baryc[1],baryc[2])
    Mrot = ECEF_to_ENU_rot_matrix(lon0,lat0)

    MRpos_loc = MRpos.copy()

    for i in range(MRpos.shape[0]):
        MRpos_loc[i,:] = np.dot(Mrot,MRpos[i,:].reshape(Mrot.shape[0],1)).flatten()

    MRpos_loc = MRpos_loc - np.mean(MRpos_loc,axis=0)
        
    return MRpos_loc, lon0, lat0, Mrot




def local_to_geocentric(MRpos,lon0,lat0,height0):
    """
    From x,y,z of N elements stored in the Nx3 array MRpos in local ENU cartesian coords, longitude, latitude and height of the barycenter, outputs x,y,z in geocentric coords.
     """

    Mrot = ECEF_to_ENU_rot_matrix(lon0,lat0)

    MRpos_geoc = MRpos.copy()

    X0,Y0,Z0 = lonlat2geocentric(lon0,lat0,height0)
    
    for i in range(MRpos.shape[0]):
        MRpos_geoc[i,:] = np.dot(Mrot.T,MRpos[i,:].reshape(Mrot.shape[0],1)).flatten() + np.array([X0,Y0,Z0])

    return MRpos_geoc




def geocentric2lonlat(x,y,z):
	"""
	From x, y ,z in meters (geocentric coords) to lon, lat, height in degrees / meters
	"""

       # projection class for lon,lat,height (deg,deg,m)
	latlong = pyproj.Proj('+proj=latlong +ellps=WGS84 +datum=WGS84 +no_defs')
       # projection class for x,y,z in a geocentric coordinates system (m)
	geocentric= pyproj.Proj('+proj=geocent +datum=WGS84 +units=m +no_defs')
        lon, lat, height = pyproj.transform(geocentric,latlong,x,y,z)

	return lon, lat, height

def lonlat2geocentric(lon,lat,height):
	"""
	From longitude, latitude in degrees, height in meters, to x, y, z (m) in geocentric coords
	"""

       # projection class for x,y,z in a geocentric coordinates system (m)
	latlong = pyproj.Proj('+proj=latlong +ellps=WGS84 +datum=WGS84 +no_defs')##4326
       # projection class for lon,lat,height (deg,deg,m)
	geocentric= pyproj.Proj('+proj=geocent +datum=WGS84 +units=m +no_defs') ##4978

        x, y, z =  pyproj.transform(latlong,geocentric,lon,lat,height)

	return x, y, z



################### Nenufar xst fits file reading  #########################################

def read_nenufar_xst_fits(filename):
	"""
	Read a nenufar XST fits file, outputs correlation matrix and some other things.
	Inputs:
              filename: full path to fits file
        Ouputs:
              MXX, MYY : complex correlation matrices NxNxNtxNf for X and Y polarization
                         (N antennas, for Nt time steps and Nf frequency bins)
              MRpos: antenna positions Nx3 in geocentric coords (m)
              noXSTFreq: list of all XST frequencies (MHz)
              subbandlist: list of Nf subband indices stored in the file
	"""

        print "> Reading headers of "+filename+" ..."
        # Read all 8 headers
        headers=[]
        for i in range(8):	
                headers.append(fitsio.read_header(filename,i))

        print "> Reading metadata ..."
        fits = fitsio.FITS(filename)
                

        # Header 0, primary extension
        date_obs = headers[0]['DATE-OBS']
        date_end = headers[0]['DATE-END']
        time_obs = headers[0]['TIME-OBS']
        time_end = headers[0]['TIME-END']

        frqrange = headers[0]['FRQRANGE'] # frequency range, list [Fmin, Fmax]
        bandwidt = headers[0]['BANDWIDT'] # bandwidth of one subband

        # Header 1, Instrument header, data extension 1
        Nf = fits[1]['Nf'].read() # total number of frequencies
        frq = fits[1]['frq'].read() # list of subband central frequencies (MHz)
        nbMR = fits[1]['nbMR'].read() # nb of mini arrays (MA)
        nbMRon = fits[1]['nbMRon'].read()[0] # nb of active MAs
#        MRpos = fits[1]['noPositionETRS'].read() # MAs position (ETRF)
        MRpos = fits[1]['noPositionETRS'].read()[0] # MAs position (ETRF)
        MRpos = np.array([MRpos[::3],MRpos[1::3],MRpos[2::3]]).T
        rotation = fits[1]['rotation'].read() # list of MAs rotations
        nbBoard = fits[1]['nbBoard'].read() # nb of cards in backend ######### !! not here in the fits!! B.C. 02/2019

        # pas de delay dans le file que je regarde ??
        #delay = fits[1]['delay'].read()

        nbPol = fits[1]['nbPol'].read() # nb of polarization channels
        noXSTFreq = fits[1]['noXSTFreq'].read() # XST frequencies list


        # Header 2, Observation header, data extension 2
        subbandlist = fits[2]['subbandlist'].read() # list of XST subbands
        dursubbandlist = fits[2]['dursubbandlist'].read() # duration for one lis        
        nbAnaBeam = fits[2]['nbAnaBeam'].read() # nb of ana beams

        # Header 3, Ana beam header, data extension 3
        target = fits[3]['Object'].read()
        Angle1 = fits[3]['Angle1'].read() # RA or azimuth (deg)
        Angle2 = fits[3]['Angle2'].read() # DEC or altitude (deg)        
        DirType = fits[3]['DirType'].read() # Pointing type, ex: "AZELGEO"
        MRList = fits[3]['MRList'].read() # Mini arrays list
        Filter = fits[3]['Filter'].read()
        FreqList = fits[3]['FreqList'].read() # Frequencies list
        NbPointingAB = fits[3]['NbPointingAB'].read() # Nb of pointings for the analog beam
        Squint = fits[3]['Squint'].read() # Beam squint (True|False)
        




        # Header 4, beam header, data extension 4
        noBeam = fits[4]['noBeam'].read() # beam number
        noAnaBeam = fits[4]['noAnaBeam'].read() # number of the analog beam associated to the beam

        # Header 7, XST data header, data extension 7
        jd = fits[7]['jd'].read() # julian date for data
        xstSubband = fits[7]['xstSubband'].read() # subband for cross correlation

        print "> Reading xst data ..."
        DATA = fits[7]['DATA'].read() # crosscorrelation statistics data        
        
        #return date_obs, date_end, time_obs, time_end, frqrange, bandwidt
        #return Nf, frq, nbMR, nbMRon, MRpos,rotation, nbPol, noXSTFreq


        print "> Put that in a matrix ..."
        #pdb.set_trace()
        MXX = np.zeros((nbMRon,nbMRon,DATA.shape[0],len(subbandlist[0])),dtype=np.complex)
        MYY = np.zeros((nbMRon,nbMRon,DATA.shape[0],len(subbandlist[0])),dtype=np.complex)        
        N = DATA.shape[0]*len(subbandlist[0])

        itot = 0
        Ndash = 40
        for i_t in range(DATA.shape[0]):
           for i_sb in range(len(subbandlist[0])):
                M = np.zeros((nbMRon*2,nbMRon*2),dtype=np.complex)
                M[np.tril_indices(nbMRon*2,0)] = DATA[i_t,i_sb,:]

                # X polar
                Mup = M[::2,::2].T.conj() # up triangle version, transpose of the lower triangle version stored in the FITS
                Mup[range(Mup.shape[0]),range(Mup.shape[1])] = 0
                MXX[:,:,i_t,i_sb] = M[::2,::2] + Mup
                # Y polar
                Mup = M[1::2,1::2].T.conj() # up triangle version, transpose of the lower triangle version stored in the FITS
                Mup[range(Mup.shape[0]),range(Mup.shape[1])] = 0
                MYY[:,:,i_t,i_sb] = M[1::2,1::2] + Mup
                


                itot+=1
                frac_done = float(itot)/N
                Ndone = int(np.round(Ndash*frac_done))

                sys.stdout.flush()
                sys.stdout.write("="*Ndone+">"+"-"*(Ndash-Ndone-1)+" ["+"%d"%(int(frac_done*100))+"%]\r")
        
        #return target, Angle1, Angle2, nbAnaBeam, DirType, MRList, Filter, FreqList, NbPointingAB, Squint

        print "\n-Done-"
        
        return MXX, MYY, MRpos, noXSTFreq, subbandlist



if __name__ == "__main__":


    parser = argparse.ArgumentParser(description='Compute image of the near field from a nenufar xst fits file.')

    parser.add_argument("filename",type=str, help="Full path to xst fits file")
    parser.add_argument("-t", "--time_index", type=int, default=0, help="Time index to be plotted")
    parser.add_argument("-f", "--freq", type=float, default=50e6, help="Frequency to be plotted (Hz)")
    parser.add_argument("-Nx", "--Nxpix", type=int, default=128, help="Nb of pixels along x dimension")
    parser.add_argument("-Ny", "--Nypix", type=int, default=128, help="Nb of pixels along y dimension")
    parser.add_argument("-d", "--dims", nargs="+",type=int, default=[-150,150,-150,150], help="Limits of the picture in meters, with syntax: -d xmin xmax ymin ymax")
    parser.add_argument("-p", "--polar", type=int, default=0, help="Polarization X or Y (0 or 1)")
    parser.add_argument("-H", "--height", type=float, default=1.5, help="Height at which image is computed")
    parser.add_argument("-o", "--output", type=str, help="Output picture file (type set by extension through `savefig()`)")
    parser.add_argument("-l", "--linear", action="store_true",help="Plot linear power instead of log")
    
#    parser.add_argument("-v", "--verbose", action = "store_true",help="Spit out the logs")

    args = parser.parse_args()
    filename = args.filename
    time_index = args.time_index
    freq = args.freq
    Nxpix = args.Nxpix
    Nypix = args.Nypix
    dims = args.dims
    polar = args.polar
    height = args.height
    output = args.output
    linear = args.linear
    
    #verbose = args.verbose


    
    MXX, MYY,MRpos, noXSTFreq,subbandlist =read_nenufar_xst_fits(filename)

    Freq = np.linspace(0.1953125,100e6,512)
    
    freq_index = np.argmin(abs(freq-Freq[subbandlist]))
    
    if polar ==0:
        M = MXX[:,:,time_index,freq_index]
    else:
        M = MYY[:,:,time_index,freq_index]


    img, dims, dims_lonlat, MRpos_loc = ground_imager(M,freq,Nxpix,Nypix,dims,MRpos,height=height,plotfig=False)


    if not linear:
        img = 10*np.log10(img/np.max(img.flatten()))
        colorbar_text = 'dB (norm to max)'
    else:
        img = img/np.max(img.flatten())
        colorbar_text = 'Linear scale (norm to max)'


    if polar == 0:
        polar_text = 'X polarization'
    else:
        polar_text = 'Y polarization'

    
    plt.figure()

    plt.imshow(img.real,interpolation='None',extent=dims,origin='lower')

    cl = plt.colorbar()
    cl.set_label(colorbar_text)

    plt.scatter(MRpos_loc[:,0],MRpos_loc[:,1], label="Antenna positions")
    plt.legend()
    plt.xlabel(r"West $\leftarrow$ x (m) $\rightarrow$ East")
    plt.ylabel(r"South $\leftarrow$ y (m) $\rightarrow$ North")
    plt.title('Ground imaging with near-field focalisation \n F = '+"%.3f"%(freq/1e6)+" MHz, "+"%d"%Nxpix+"x"+"%d"%Nypix+" pixels , pixel size = "+"%.1f"%(float(dims[1]-dims[0])/Nxpix)+" m\n"+polar_text)
    plt.gca().set_aspect('auto')
    

    
    if output:
        
        plt.savefig(output,bbox_inches='tight')
        
    plt.show()
